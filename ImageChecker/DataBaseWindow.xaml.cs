﻿using System;
using System.Linq;
using System.Windows;


namespace ImageChecker
{
	/// <summary>
	/// Interaction logic for DataBaseWindow.xaml
	/// </summary>
	public partial class DataBaseWindow : Window
	{
		public readonly MainWindow mainWindow;
		private readonly DataBase.AppContext db;
		public DataBaseWindow(MainWindow window)
		{
			InitializeComponent();
			mainWindow = window;
			db = new DataBase.AppContext();
			if (db.Images.Any())
			{
				DataGrid.ItemsSource = db.Images.ToList();
			}
		}

		private void OpenImage_Click(object sender, RoutedEventArgs e)
		{
			var cell = DataGrid.SelectedCells.FirstOrDefault();
			var image = (DataBase.Image) cell.Item;
			mainWindow.ShowImageDetails(image.GetImage(), image.Name);
		}

		private void DeleteImage_Click(object sender, RoutedEventArgs e)
		{
			var cell = DataGrid.SelectedCells.FirstOrDefault();
			var image = (DataBase.Image)cell.Item;
			db.Images.Remove(image);
			db.SaveChanges();
			if (db.Images.Any())
			{
				DataGrid.ItemsSource = db.Images.ToList();
			}
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			db.Dispose();
		}
	}
}
