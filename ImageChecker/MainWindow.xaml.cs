﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using ImageChecker.Functions;
using ImageChecker.DataBase;
using Microsoft.Win32;
using AppContext = ImageChecker.DataBase.AppContext;


namespace ImageChecker
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private BitmapImage currentImage;

		public MainWindow()
		{
			InitializeComponent();
			currentImage = null;
			if (!DbCore.DbExists())
			{
				DbCore.CreateDb();
			}
		}

		#region FileMenuClicks

		private void OpenImage_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog {Multiselect = false};
			if (openFileDialog.ShowDialog() == true)
			{
				var image = new BitmapImage(new Uri(openFileDialog.FileName));
				currentImage = image;
				ShowImageDetails(image, openFileDialog.SafeFileName);
			}
				
		}

		public void ShowImageDetails(BitmapSource image, string imageName)
		{
			var grayscale = image is null ? null : CoreFunctions.GrayScale(image);
			SourceImage.Source = image;
			GrayscalseImage.Source = grayscale;
			ImageName.Text = imageName;
			AvgBrightness.Text = image is null ? "0" : CoreFunctions.WhiteBalance(image).ToString();
			AvgColor.Text = image is null ? "0" : CoreFunctions.MidColor(image).ToString();
			AvgGrayscale.Text = image is null ? "0" : CoreFunctions.MidColor(grayscale).ToString();
		}

		private void ClearImage_Click(object sender, RoutedEventArgs e)
		{
			ShowImageDetails(null, "");
		}

		private void AddToDatabase_Click(object sender, RoutedEventArgs e)
		{
			if (currentImage != null)
			{
				var db = new DataBase.AppContext();
				var img = new DataBase.Image(currentImage, ImageName.Text);
				db.Images.Add(img);
				db.SaveChanges();
				db.Dispose();
			}
		}

		private void Exit_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		#endregion

		#region DatabaseMenuClicks

		private void OpenDatabase_Click(object sender, RoutedEventArgs e)
		{
			var databaseWindow = new DataBaseWindow(this);
			databaseWindow.Show();
		}

		private void ClearDatabase_Click(object sender, RoutedEventArgs e)
		{
			DbCore.RecreateDb();
		}

		#endregion

		private void OpenHelpWindow_Click(object sender, RoutedEventArgs e)
		{
			var helpWindow = new HelpWindow();
			helpWindow.Show();
		}
	}
}
