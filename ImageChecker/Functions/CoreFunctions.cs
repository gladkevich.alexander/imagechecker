﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

//using System.Drawing;

namespace ImageChecker.Functions
{
	static class CoreFunctions
	{
		/// <summary>
		/// Сохранение изображения на диск
		/// </summary>
		/// <param name="filename">Название файла, в т.ч. его расширение</param>
		/// <param name="image"></param>
		public static void SaveImage(string filename, BitmapSource image)
		{
			if (filename != string.Empty)
			{
				using (var stream5 = new FileStream(filename, FileMode.Create))
				{
					var encoder5 = new PngBitmapEncoder();
					encoder5.Frames.Add(BitmapFrame.Create(image));
					encoder5.Save(stream5);
				}
			}
		}

		/// <summary>
		/// Расчет средней яркости всех пикселей изображения
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		public static double WhiteBalance(BitmapSource image)
		{
			var buffer = GetPixels(image);

			double res = 0;
			Parallel.For(0, buffer.Length / 4, (i, state) =>
			{
				res += 0.2126 * buffer[i + 2] + 0.7152 * buffer[i + 1] + 0.0722 * buffer[i];
			});

			return res / (buffer.Length / 4.0); ;
		}

		//BUG: Изображение выводится в виде 4х повторяющихся.
		/// <summary>
		/// Конвертация цветного изображения в оттенки серого
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		public static BitmapSource GrayScale(BitmapSource image)
		{
			var grayscaleBitmap = new FormatConvertedBitmap(image, PixelFormats.Gray8, null, 0d);

			var rect = new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight);
			var bufferGray = GetPixels(grayscaleBitmap);
			var buffer = new byte[bufferGray.Length * 4];
			Parallel.For(0, bufferGray.Length, (i, state) =>
			{
				buffer[i] = bufferGray[i];
				buffer[i + 1] = bufferGray[i];
				buffer[i + 2] = bufferGray[i];
			});

			var res = new WriteableBitmap(image.PixelWidth, image.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
			res.WritePixels(rect, buffer, buffer.Length / image.PixelHeight, 0);
			return res;
		}

		/// <summary>
		/// Возвращает среднее значение цвета всех пикселей в формате RGB
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		public static Color MidColor(BitmapSource image)
		{
			var buffer = GetPixels(image);
			double R = 0;
			double G = 0;
			double B = 0;

			for (int i = 4; i < buffer.Length; i += 4)
			{
				B += buffer[i - 4];
				G += buffer[i - 3];
				R += buffer[i - 2];
			}

			B /= buffer.Length / 4.0;
			G /= buffer.Length / 4.0;
			R /= buffer.Length / 4.0;

			return new Color { R = (byte)R, G = (byte)G, B = (byte)B };
		}

		public static void CreateThumbnail(string filename, BitmapSource image)
		{
			if (filename != string.Empty)
			{
				using (var stream = new FileStream(filename, FileMode.Create))
				{
					var encoder = new PngBitmapEncoder();
					encoder.Frames.Add(BitmapFrame.Create(image));
					encoder.Save(stream);
				}
			}
		}

		public static byte[] GetPixels(BitmapSource image)
		{
			var bytesPerPixel = (image.Format.BitsPerPixel + 7) / 8;
			var stride = image.PixelWidth * bytesPerPixel;
			var buffer = new byte[image.PixelHeight * stride];

			image.CopyPixels(buffer, stride, 0);
			return buffer;
		}
	}
}
