﻿using System.Data.Entity;


namespace ImageChecker.DataBase
{
	class AppContext : DbContext
	{
		public AppContext() : base("DefaultConnection")
		{
		}

		public DbSet<Image> Images { get; set; }
	}
}
