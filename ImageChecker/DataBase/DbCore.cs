﻿using System.Data.SQLite;
using System.IO;

namespace ImageChecker.DataBase
{
	class DbCore
	{
		private static string dbName = "ApplicationDb.db";

		public static void DeleteDb()
		{
			if (File.Exists(dbName))
			{
				File.Delete(dbName);
			}
		}

		public static AppContext RecreateDb()
		{
			DeleteDb();
			return CreateDb();
		}

		public static bool DbExists()
		{
			return File.Exists(dbName);
		}

		public static AppContext CreateDb()
		{
			if (DbExists())
			{
				return new AppContext();
			}
			SQLiteConnection.CreateFile(dbName);
			using (SQLiteConnection connect = new SQLiteConnection($"Data Source=.\\{dbName}"))
			{
				string commandText = "CREATE TABLE Images ("+
				"Id INTEGER PRIMARY KEY NOT NULL,"+
				"Name        TEXT NOT NULL,"+
				"Value		 BLOB NOT NULL,"+
				"PixelWidth  INTEGER NOT NULL," +
				"PixelHeight INTEGER NOT NULL," +
				"DpiX        DOUBLE NOT NULL,"+
				"DpiY DOUBLE  NOT NULL"+
				");"; 

				SQLiteCommand command = new SQLiteCommand(commandText, connect);
				connect.Open();
				command.ExecuteNonQuery();
				connect.Close();
			}
			return new AppContext();
		}
	}
}
