﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ImageChecker.Functions;

namespace ImageChecker.DataBase
{
	/// <summary>
	/// Класс для хранения изображения в БД
	/// </summary>
	class Image
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public byte[] Value { get; set; }
		public int PixelWidth { get; set; }
		public int PixelHeight { get; set; }
		public double DpiX { get; set; }
		public double DpiY { get; set; }

		public Image(BitmapSource image, string name)
		{
			Value = CoreFunctions.GetPixels(image);
			Name = name;
			PixelWidth = image.PixelWidth;
			PixelHeight = image.PixelHeight;
			DpiX = image.DpiX;
			DpiY = image.DpiY;
		}

		public Image(string imageAbsolutePath, string name)
		{
			var image = new BitmapImage(new Uri(imageAbsolutePath, UriKind.Absolute));
			Value = CoreFunctions.GetPixels(image);
			Name = name;
			PixelWidth = image.PixelWidth;
			PixelHeight = image.PixelHeight;
			DpiX = image.DpiX;
			DpiY = image.DpiY;
		}

		public Image()
		{
			Value = null;
			Name = null;
			PixelWidth = 0;
			PixelHeight = 0;
			DpiX = 0;
			DpiY = 0;
		}

		public WriteableBitmap GetImage()
		{
			var img = new WriteableBitmap(PixelWidth, PixelHeight, DpiX, DpiY, PixelFormats.Bgr32, null);
			var rect = new Int32Rect(0, 0, PixelWidth, PixelHeight);
			img.WritePixels(rect, Value, Value.Length / PixelHeight, 0);
			return img;
		}
	}
}
